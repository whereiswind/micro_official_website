package top.yanxiaozhao.data.service;

import top.yanxiaozhao.data.domain.User;

public interface UserService {
    User selectByOpenid(String openid);
    void insert(User user);
}
