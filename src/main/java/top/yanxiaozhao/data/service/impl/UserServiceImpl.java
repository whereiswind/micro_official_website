package top.yanxiaozhao.data.service.impl;

import top.yanxiaozhao.data.domain.User;
import top.yanxiaozhao.data.mapper.UserMapper;
import top.yanxiaozhao.data.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.yanxiaozhao.data.domain.User;
import top.yanxiaozhao.data.mapper.UserMapper;

@Service
public class UserServiceImpl implements UserService {
    private UserMapper userMapper;

    @Autowired(required = false)
    public void setUserMapper(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    @Override
    public User selectByOpenid(String openid) {
        return userMapper.selectByOpenid(openid);
    }

    @Override
    public void insert(User user) {
        userMapper.insert(user);
    }
}
