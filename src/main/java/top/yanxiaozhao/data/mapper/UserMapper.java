package top.yanxiaozhao.data.mapper;

import top.yanxiaozhao.data.domain.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper {
    User selectByOpenid(String openid);
    void insert(User user);
}
