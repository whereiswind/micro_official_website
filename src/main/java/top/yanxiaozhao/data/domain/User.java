package top.yanxiaozhao.data.domain;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class User {
    private Integer id;
    private String openid;
    private String skey;
    private String nickname;
    private Integer gender;
    private String avatarUrl;
    private Timestamp createTime;

    public User(String openid, String skey, String nickname, int gender, String avatarUrl) {
        this.id = null;
        this.openid = openid;
        this.skey = skey;
        this.nickname = nickname;
        this.gender = gender;
        this.avatarUrl = avatarUrl;
    }
}
